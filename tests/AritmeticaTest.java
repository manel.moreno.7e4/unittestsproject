import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AritmeticaTest {
    Aritmetica a = new Aritmetica();
    @Test
    void TestSuma() {
        assertEquals(6, a.Suma(3, 3));
    }

    @Test
    void TestResta() {
        assertEquals(7, a.Resta(10, 3));
    }

    @Test
    void TestDivisioIntMesPetit() {
        assertEquals(1, a.Divisio(8, 5));
    }

    @Test
    void TestDivisioIntMesGran() {
        assertEquals(0, a.Divisio(5, 8));
    }

    @Test
    void TestDivisioIntEntreZero() {
        assertEquals(0, a.Divisio(10, 0));
    }

    @Test
    void TestDivisioIntNegatius() {
        assertEquals(-2, a.Divisio(-4, 2));
    }

    @Test
    void TestMaxim() {
        assertEquals(6, a.Maxim(5, 2, 6));
    }
}